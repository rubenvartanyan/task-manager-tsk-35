package ru.vartanyan.tm.exception.system;

public class AccessDeniedException extends Exception{

    public AccessDeniedException() throws Exception {
        super("Error! Access denied...");
    }

}
