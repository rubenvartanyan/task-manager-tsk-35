package ru.vartanyan.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-04-06T15:34:29.332+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.vartanyan.ru/", name = "TaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByNameRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByNameResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByName/Fault/Exception")})
    @RequestWrapper(localName = "removeTaskByName", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.RemoveTaskByName")
    @ResponseWrapper(localName = "removeTaskByNameResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.RemoveTaskByNameResponse")
    public void removeTaskByName(

        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findAllTasksRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findAllTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findAllTasks/Fault/Exception")})
    @RequestWrapper(localName = "findAllTasks", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindAllTasks")
    @ResponseWrapper(localName = "findAllTasksResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindAllTasksResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.vartanyan.tm.endpoint.Task> findAllTasks(

        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByIndexRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByIndexResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByIndex/Fault/Exception")})
    @RequestWrapper(localName = "findTaskByIndex", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindTaskByIndex")
    @ResponseWrapper(localName = "findTaskByIndexResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vartanyan.tm.endpoint.Task findTaskByIndex(

        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByIdRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskById/Fault/Exception")})
    @RequestWrapper(localName = "removeTaskById", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.RemoveTaskById")
    @ResponseWrapper(localName = "removeTaskByIdResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.RemoveTaskByIdResponse")
    public void removeTaskById(

        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByIndexRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByIndexResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/removeTaskByIndex/Fault/Exception")})
    @RequestWrapper(localName = "removeTaskByIndex", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.RemoveTaskByIndex")
    @ResponseWrapper(localName = "removeTaskByIndexResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.RemoveTaskByIndexResponse")
    public void removeTaskByIndex(

        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/addTaskRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/addTaskResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/addTask/Fault/Exception")})
    @RequestWrapper(localName = "addTask", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.AddTask")
    @ResponseWrapper(localName = "addTaskResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.AddTaskResponse")
    public void addTask(

        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/clearTasksRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/clearTasksResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/clearTasks/Fault/Exception")})
    @RequestWrapper(localName = "clearTasks", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.ClearTasks")
    @ResponseWrapper(localName = "clearTasksResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.ClearTasksResponse")
    public void clearTasks(

        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByIdRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskById/Fault/Exception")})
    @RequestWrapper(localName = "findTaskById", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindTaskById")
    @ResponseWrapper(localName = "findTaskByIdResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vartanyan.tm.endpoint.Task findTaskById(

        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByNameRequest", output = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByNameResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.tm.vartanyan.ru/TaskEndpoint/findTaskByName/Fault/Exception")})
    @RequestWrapper(localName = "findTaskByName", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindTaskByName")
    @ResponseWrapper(localName = "findTaskByNameResponse", targetNamespace = "http://endpoint.tm.vartanyan.ru/", className = "ru.vartanyan.tm.endpoint.FindTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vartanyan.tm.endpoint.Task findTaskByName(

        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "session", targetNamespace = "")
        ru.vartanyan.tm.endpoint.Session session
    ) throws Exception_Exception;
}
