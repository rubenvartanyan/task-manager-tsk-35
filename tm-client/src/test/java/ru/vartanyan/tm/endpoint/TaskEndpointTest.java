package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class TaskEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addTaskTest() {
        final String taskName = "nameTest";
        final String taskDescription = "nameTest";
        endpointLocator.getTaskEndpoint().addTask(taskName, taskDescription, session);
        final Task task = endpointLocator.getTaskEndpoint().findTaskByName(taskName, session);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getId());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllTaskTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        taskEndpoint.addTask("task2", "description2", session);
        taskEndpoint.addTask("task3", "description3", session);
        Assert.assertEquals(3, taskEndpoint.findAllTasks(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskByIdTest() {
        endpointLocator.getTaskEndpoint().clearTasks(session);
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        final Task task = taskEndpoint.findTaskByName("task1", session);
        final Task taskFound = taskEndpoint.findTaskById(task.getId(), session);
        Assert.assertNotNull(taskFound);
        Assert.assertEquals(task, taskFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIndexTest() {
        endpointLocator.getTaskEndpoint().clearTasks(session);
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        final Task task = taskEndpoint.findTaskByName("task1", session);
        final Task taskFound = taskEndpoint.findTaskByIndex(0, session);
        Assert.assertNotNull(taskFound);
        Assert.assertEquals(task, taskFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByNameTest() {
        endpointLocator.getTaskEndpoint().clearTasks(session);
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        final Task task = taskEndpoint.findTaskByIndex(0, session);
        final Task taskFound = taskEndpoint.findTaskByName("task1", session);
        Assert.assertNotNull(taskFound);
        Assert.assertEquals(task, taskFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        final Task task = taskEndpoint.findTaskByName("task1", session);
        final String taskId = task.getId();
        Assert.assertNotNull(taskEndpoint.findTaskById(taskId, session));
        taskEndpoint.removeTaskById(taskId, session);
        Assert.assertNull(taskEndpoint.findTaskById(taskId, session));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskByNameTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        Assert.assertNotNull(taskEndpoint.findTaskByName("task1", session));
        taskEndpoint.removeTaskByName("task1", session);
        Assert.assertNull(taskEndpoint.findTaskByName("task1", session));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask("task1", "description1", session);
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(0, session));
        taskEndpoint.removeTaskByIndex(0, session);
        Assert.assertNull(taskEndpoint.findTaskByIndex(0, session));
    }

}
