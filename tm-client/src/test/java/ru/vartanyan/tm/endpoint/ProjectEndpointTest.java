package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class ProjectEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addProjectTest() {
        final String projectName = "nameTest";
        final String projectDescription = "nameTest";
        endpointLocator.getProjectEndpoint().addProject(projectName, projectDescription, session);
        final Project project = endpointLocator.getProjectEndpoint().findProjectByName(projectName, session);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getId());
        Assert.assertEquals(projectDescription, project.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllProjectTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        projectEndpoint.addProject("project2", "description2", session);
        projectEndpoint.addProject("project3", "description3", session);
        Assert.assertEquals(3, projectEndpoint.findAllProjects(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectByIdTest() {
        endpointLocator.getProjectEndpoint().clear(session);
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        final Project project = projectEndpoint.findProjectByName("project1", session);
        final Project projectFound = projectEndpoint.findProjectById(project.getId(), session);
        Assert.assertNotNull(projectFound);
        Assert.assertEquals(project, projectFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIndexTest() {
        endpointLocator.getProjectEndpoint().clear(session);
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        final Project project = projectEndpoint.findProjectByName("project1", session);
        final Project projectFound = projectEndpoint.findProjectByIndex(0, session);
        Assert.assertNotNull(projectFound);
        Assert.assertEquals(project, projectFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByNameTest() {
        endpointLocator.getProjectEndpoint().clear(session);
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        final Project project = projectEndpoint.findProjectByIndex(0, session);
        final Project projectFound = projectEndpoint.findProjectByName("project1", session);
        Assert.assertNotNull(projectFound);
        Assert.assertEquals(project, projectFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIdTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        final Project project = projectEndpoint.findProjectByName("project1", session);
        final String projectId = project.getId();
        Assert.assertNotNull(projectEndpoint.findProjectById(projectId, session));
        projectEndpoint.removeProjectById(projectId, session);
        Assert.assertNull(projectEndpoint.findProjectById(projectId, session));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectByNameTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        Assert.assertNotNull(projectEndpoint.findProjectByName("project1", session));
        projectEndpoint.removeProjectByName("project1", session);
        Assert.assertNull(projectEndpoint.findProjectByName("project1", session));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectByIndexTest() {
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        projectEndpoint.addProject("project1", "description1", session);
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(0, session));
        projectEndpoint.removeProjectByIndex(0, session);
        Assert.assertNull(projectEndpoint.findProjectByIndex(0, session));
    }

}
