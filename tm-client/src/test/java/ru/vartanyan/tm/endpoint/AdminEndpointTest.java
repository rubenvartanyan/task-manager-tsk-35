package ru.vartanyan.tm.endpoint;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class AdminEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        endpointLocator.getAdminEndpoint().removeUserByLogin("test1", sessionAdmin);
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin("test2", session));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByIdTest() {
        final User user = new User();
        final String userId = user.getId();
        final User userFound = endpointLocator.getAdminEndpoint().findUserById(userId, sessionAdmin);
        Assert.assertEquals(user, userFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllUsersTest() {
        endpointLocator.getAdminEndpoint().clearUsers(sessionAdmin);
        final User user1 = new User();
        final User user2 = new User();
        final List<User> userList = endpointLocator.getAdminEndpoint().findAllUsers(sessionAdmin);
        Assert.assertEquals(2, userList.size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLogin() {
        final User user = new User();
        final String login = user.getLogin();
        final User userFound = endpointLocator.getAdminEndpoint().findUserByItsLogin(login, sessionAdmin);
        Assert.assertEquals(user, userFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void clearUsersTest() {
        endpointLocator.getAdminEndpoint().clearUsers(sessionAdmin);
        final User user1 = new User();
        final User user2 = new User();
        endpointLocator.getAdminEndpoint().addUser(user1, sessionAdmin);
        endpointLocator.getAdminEndpoint().addUser(user2, sessionAdmin);
        endpointLocator.getAdminEndpoint().clearUsers(sessionAdmin);
        final List<User> userList = endpointLocator.getAdminEndpoint().findAllUsers(sessionAdmin);
        Assert.assertEquals(0, userList.size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithEmailTest() {
        endpointLocator.getAdminEndpoint().createUserWithEmail("test1", "test1", "test@tsk.ru", sessionAdmin);
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        endpointLocator.getAdminEndpoint().createUser("login1", "password1", sessionAdmin);
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithRoleTest() {
        endpointLocator.getAdminEndpoint().createUserWithRole("login1", "password1", Role.USER, sessionAdmin);
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        User user1 = new User();
        user1.setLogin("login1");
        endpointLocator.getAdminEndpoint().lockUserByLogin("login1", sessionAdmin);
        Assert.assertTrue(user1.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByIdTest() {
        User user1 = new User();
        endpointLocator.getAdminEndpoint().lockUserById(user1.getId(), sessionAdmin);
        Assert.assertTrue(user1.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        User user1 = new User();
        user1.setLogin("login1");
        endpointLocator.getAdminEndpoint().lockUserByLogin("login1", sessionAdmin);
        Assert.assertTrue(user1.locked);
        endpointLocator.getAdminEndpoint().unlockUserByLogin("login1", sessionAdmin);
        Assert.assertFalse(user1.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByIdTest() {
        User user1 = new User();
        endpointLocator.getAdminEndpoint().lockUserById(user1.getId(), sessionAdmin);
        Assert.assertTrue(user1.locked);
        endpointLocator.getAdminEndpoint().unlockUserById(user1.getId(), sessionAdmin);
        Assert.assertFalse(user1.locked);
    }


    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByIdTest() {
        User user1 = new User();
        endpointLocator.getAdminEndpoint().addUser(user1, sessionAdmin);
        endpointLocator.getAdminEndpoint().removeUserById(user1.getId(), sessionAdmin);
        Assert.assertNull(endpointLocator.getAdminEndpoint().findUserByItsLogin(user1.getId(), sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        User user1 = new User();
        user1.setLogin("login1");
        endpointLocator.getAdminEndpoint().addUser(user1, sessionAdmin);
        endpointLocator.getAdminEndpoint().removeUserByLogin("login1", sessionAdmin);
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin("login1", sessionAdmin));
    }

}
