package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        User user1 = new User();
        user1.setLogin("login1");
        endpointLocator.getAdminEndpoint().addUser(user1, sessionAdmin);
        endpointLocator.getUserEndpoint().removeUserByItsLogin("login1", sessionAdmin);
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin("login1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLogin() {
        final User user = new User();
        final String login = user.getLogin();
        final User userFound = endpointLocator.getUserEndpoint().findUserByLogin(login, sessionAdmin);
        Assert.assertEquals(user, userFound);
    }

}
