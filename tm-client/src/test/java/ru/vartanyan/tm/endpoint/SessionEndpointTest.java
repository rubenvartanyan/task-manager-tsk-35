package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class SessionEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void openSessionTest() {
        final Session session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        Assert.assertNotNull(session.getUserId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void closeSessionTest() {
        final Session session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        Assert.assertNotNull(session);
        endpointLocator.getSessionEndpoint().closeSession(session);
        Assert.assertNull(session.getUserId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void listSession() {
        final Session session1 = endpointLocator.getSessionEndpoint().openSession("test", "test");
        final List<Session> listSession = endpointLocator.getSessionEndpoint().listSession();
        Assert.assertEquals(2, listSession.size());
    }

}
