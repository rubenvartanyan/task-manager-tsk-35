package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.service.ServiceLocator;

public class AbstractEndpoint {

    @NotNull final ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
