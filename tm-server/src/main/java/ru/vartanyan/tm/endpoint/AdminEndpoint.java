package ru.vartanyan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.endpoint.IAdminEndpoint;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.Session;
import ru.vartanyan.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Override
    public User findUserById(@WebParam (name = "id", partName = "id") @NotNull String id,
                             @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @WebMethod
    @Override
    public void addUser(@WebParam (name = "user", partName = "user") @NotNull User user,
                        @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().add(user);
    }

    @WebMethod
    @Override
    public User findUserByItsLogin(@WebParam (name = "login", partName = "login") @NotNull String login,
                                   @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    @Override
    public void removeUser(@WebParam (name = "user", partName = "user") @NotNull User user,
                           @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().remove(user);
    }

    @WebMethod
    @Override
    public void removeUserById(@WebParam (name = "id", partName = "id") @NotNull String id,
                               @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

    @WebMethod
    @Override
    public void removeUserByLogin(@WebParam (name = "login", partName = "login") @NotNull String login,
                                  @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @WebMethod
    @Override
    public @NotNull User createUser(@WebParam (name = "login", partName = "login") @NotNull String login,
                                    @WebParam (name = "password", partName = "password") @NotNull String password,
                                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password);
    }

    @WebMethod
    @Override
    public void createUserWithEmail(@WebParam (name = "login", partName = "login") @NotNull String login,
                                    @WebParam (name = "password", partName = "password") @NotNull String password,
                                    @WebParam (name = "email", partName = "email") @NotNull String email,
                                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, email);
    }

    @WebMethod
    @Override
    public void createUserWithRole(@WebParam (name = "login", partName = "login") @NotNull String login,
                                   @WebParam (name = "password", partName = "password") @NotNull String password,
                                   @WebParam (name = "role", partName = "role") @NotNull Role role,
                                   @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().create(login, password, role);
    }

    @WebMethod
    @Override
    public void setUserPassword(@WebParam (name = "userId", partName = "userId") @NotNull String userId,
                                @WebParam (name = "password", partName = "password") @NotNull String password,
                                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @WebMethod
    @Override
    public void updateUser(@WebParam (name = "userId", partName = "userId") @NotNull String userId,
                           @WebParam (name = "firstName", partName = "firstName") @Nullable String firstName,
                           @WebParam (name = "lastName", partName = "lastName") @Nullable String lastName,
                           @WebParam (name = "middleName", partName = "middleName") @Nullable String middleName,
                           @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @WebMethod
    @Override
    public void unlockUserByLogin(@WebParam (name = "login", partName = "login") @NotNull String login,
                                  @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @WebMethod
    @Override
    public void unlockUserById(@WebParam (name = "id", partName = "id") @NotNull String id,
                               @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserById(id);
    }

    @WebMethod
    @Override
    public void lockUserByLogin(@WebParam (name = "login", partName = "login") @NotNull String login,
                                @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @WebMethod
    @Override
    public void lockUserById(@WebParam (name = "id", partName = "id") @NotNull String id,
                             @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserById(id);
    }

    @WebMethod
    @Override
    public @Nullable List<User> findAllUsers(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    @Override
    public void clearUsers(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().clear();

    }

    @WebMethod
    @Override
    public void addAllUsers(@WebParam (name = "users", partName = "users") @NotNull List<User> users,
                            @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session, Role.ADMIN);
        serviceLocator.getUserService().addAll(users);
    }

}
