package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Comparator;
import java.util.List;

public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    Project findProjectById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                            @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeProjectById(@WebParam (name = "id", partName = "id") @NotNull final String id,
                    @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    @NotNull
    List<Project> findAllProjects(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    void clear(@WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    @Nullable
    Project findProjectByIndex(@WebParam (name = "index", partName = "index") @NotNull Integer index,
                               @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    @Nullable
    Project findProjectByName(@WebParam (name = "name", partName = "name") @NotNull final String name,
                              @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeProjectByIndex(@WebParam (name = "index", partName = "index") @NotNull final Integer index,
                          @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void removeProjectByName(@WebParam (name = "name", partName = "name") @NotNull final String name,
                         @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

    @WebMethod
    void addProject(@WebParam (name = "name", partName = "name") @NotNull String name,
                           @WebParam (name = "description", partName = "description") @NotNull String description,
                           @WebParam (name = "session", partName = "session") @NotNull Session session) throws Exception;

}
