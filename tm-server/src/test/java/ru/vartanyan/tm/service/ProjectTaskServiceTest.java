package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.ProjectRepository;
import ru.vartanyan.tm.repository.TaskRepository;

import java.util.List;

public class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Test
    @Category(UnitCategory.class)
    public void findAllTasksByProjectIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final String projectId = project.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(projectId);
        task2.setProjectId(projectId);
        final List<Task> taskList = projectTaskService.findAllTaskByProjectId(projectId, userId);
        Assert.assertEquals(2, taskList.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAllByProjectIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final String projectId = project.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(projectId);
        task2.setProjectId(projectId);
        projectTaskService.removeProjectById(projectId, userId);
        final List<Task> taskList = projectTaskService.findAllTaskByProjectId(projectId, userId);
        Assert.assertEquals(0, taskList.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        final String projectId = project.getId();
        final Task task = new Task();
        final String taskId = task.getId();
        projectTaskService.bindTaskByProjectId(projectId, taskId, userId);
        Assert.assertEquals(task, projectTaskService.findAllTaskByProjectId(projectId, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskFromProjectTest() throws Exception {
        final User user = new User();
        final String userId = user.getId();
        final Project project = new Project();
        final String projectId = project.getId();
        final Task task = new Task();
        final String taskId = task.getId();
        projectTaskService.bindTaskByProjectId(projectId, taskId, userId);
        Assert.assertEquals(task, projectTaskService.findAllTaskByProjectId(projectId, userId));
        projectTaskService.unbindTaskFromProject(projectId, taskId, userId);
        Assert.assertNull(projectTaskService.findAllTaskByProjectId(projectId, userId));
    }


}
