package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.repository.TaskRepository;
import ru.vartanyan.tm.service.TaskService;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Test
    @Category(UnitCategory.class)
    public void addAllTasksTest() throws Exception {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertNotNull(taskService.findById(task1.getId()));
        Assert.assertNotNull(taskService.findById(task2.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void addTaskTest() throws Exception {
        final Task task = new Task();
        taskService.add(task);
        Assert.assertNotNull(taskService.findById(task.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTasksTest() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTasks() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertEquals(2, taskService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByIdTest() throws Exception {
        final Task task = new Task();
        final String taskId = task.getId();
        taskService.add(task);
        Assert.assertNotNull(taskService.findById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByIndexTest() throws Exception {
        taskService.clear();
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertEquals(task, taskService.findOneByIndex(0, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskByNameTest() throws Exception {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertEquals(task, taskService.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIdTest() throws Exception {
        final Task task1 = new Task();
        taskService.add(task1);
        final String taskId = task1.getId();
        taskService.removeById(taskId);
        Assert.assertNull(taskService.findById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByNameTest() throws Exception {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskService.add(task);
        taskService.removeOneByName(name, userId);
        Assert.assertNull(taskService.findOneByName(name, userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIndexTest() throws Exception {
        taskService.clear();
        final Task task = new Task();
        final String taskId = task.getId();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("task1");
        final String name = task.getName();
        taskService.add(task);
        taskService.removeOneByIndex(0, userId);
        Assert.assertNull(taskService.findById(name, taskId));
    }

}
